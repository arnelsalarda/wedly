﻿using System;
using Foundation;
using UIKit;
using Wedly.iOS.DependencyServices;
using Wedly.Utilities.DependecyServices;
using Xamarin.Forms;

[assembly: Dependency(typeof(ToastDependency))]
namespace Wedly.iOS.DependencyServices
{
	public class ToastDependency : iToast
    {
        NSTimer alertDelay;
        UIAlertController alert;

        public void ShowMessage(string message)
        {
            ShowAlert(message, 3.5);
        }

        void ShowAlert(string message, double seconds)
        {
            alertDelay = NSTimer.CreateScheduledTimer(seconds, (obj) =>
            {
                DismissMessage();
            });
            alert = UIAlertController.Create(null, message, UIAlertControllerStyle.Alert);
            UIApplication.SharedApplication.KeyWindow.RootViewController.PresentViewController(alert, true, null);
        }

        void DismissMessage()
        {
            if (alert != null)
            {
                alert.DismissViewController(true, null);
            }
            if (alertDelay != null)
            {
                alertDelay.Dispose();
            }
        }
    }
}
