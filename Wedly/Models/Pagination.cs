﻿using System;
namespace Wedly.Models
{
    public class Pagination
    {
        public string events { get; set; }
        public int off_set { get; set; }
        public int load_more { get; set; }
        public string url { get; set; }
        public int total_attendees { get; set; }
        public int total_comments { get; set; }
    }
}
