﻿using System;
using System.Collections.ObjectModel;
using SQLite;

namespace Wedly.Models
{
    public class Plan
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string start_date { get; set; }
        public string start_time { get; set; }
        public string officiator { get; set; }
        public string reception_address { get; set; }
        public string wedding_address { get; set; }
        public int is_owner { get; set; }
        public string banner { get; set; }
        public string groom_name { get; set; }
        public string groom_photo { get; set; }
        public string bride_name { get; set; }
        public string bride_photo { get; set; }
        public int rsvp { get; set; }
        public int total_attendee { get; set; }
        public ObservableCollection<User> attendee { get; set; }
        public ObservableCollection<Comment> comments { get; set; }
    }
}
