﻿using System;
using System.Collections.ObjectModel;

namespace Wedly.Models
{
    public class Comment
    {
        public int id { get; set; }
        public string content { get; set; }
        public string image { get; set; }
        public string name { get; set; }
        public string avatar { get; set; }
    }
}
