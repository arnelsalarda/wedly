﻿using System;
using SQLite;

namespace Wedly.Models
{
    public class User
    {
        public int id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string birthday { get; set; }
        public string avatar { get; set; }
        public int age { get; set; }
        public int gender { get; set; }
        public int rsvp { get; set; }
        public string role { get; set; }
        public string token { get; set; }
    }
}
