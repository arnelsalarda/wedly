﻿using System;
namespace Wedly.Models
{
    public class Attendee
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public string role { get; set; }
        public int rsvp { get; set; }
    }
}
