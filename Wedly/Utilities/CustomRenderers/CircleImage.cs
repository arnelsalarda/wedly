﻿using System;
using Xamarin.Forms;

namespace Wedly.Utilities.CustomRenderers
{
    public class CircleImage : Image
    {
        public static readonly BindableProperty BorderTintProperty = BindableProperty.Create("BorderTint", typeof(Color), typeof(CircleImage), Color.White);
        public static readonly BindableProperty FillColorProperty = BindableProperty.Create("FillColor", typeof(Color), typeof(CircleImage), Color.Black);
        public static readonly BindableProperty BorderThicknessProperty = BindableProperty.Create("BorderThickness", typeof(int), typeof(CircleImage), 1);

        public Color FillColor
        {
            get { return (Color)GetValue(FillColorProperty); }
            set { SetValue(FillColorProperty, value); }
        }

        public Color BorderTint
        {
            get { return (Color)GetValue(BorderTintProperty); }
            set { SetValue(BorderTintProperty, value); }
        }

        public int BorderThickness
        {
            get { return (int)GetValue(BorderThicknessProperty); }
            set { SetValue(BorderThicknessProperty, value); }
        }
    }
}
