﻿using System;
using Xamarin.Forms;

namespace Wedly.Utilities.CustomRenderers
{
    public class CustomEntry : Entry
    {
        public static readonly BindableProperty AutoCapitalizationProperty = BindableProperty.Create("AutoCapitalization", typeof(string), typeof(CustomEntry), "None");
        public static readonly BindableProperty TextAlignmentProperty = BindableProperty.Create("TextAlignment", typeof(string), typeof(CustomEntry), "None");

        public string AutoCapitalization
        {
            get { return (string)GetValue(AutoCapitalizationProperty); }
            set { SetValue(AutoCapitalizationProperty, value); }
        }

        public string TextAlignment
        {
            get { return (string)GetValue(TextAlignmentProperty); }
            set { SetValue(TextAlignmentProperty, value); }
        }
    }
}
