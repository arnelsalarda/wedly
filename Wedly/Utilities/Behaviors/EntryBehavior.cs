﻿using System;
using Xamarin.Forms;

namespace Wedly.Utilities.Behaviors
{
    public class EntryBehavior : Behavior<Entry>
    {
        string entryName;

        protected override void OnAttachedTo(Entry bindable)
        {
            bindable.Focused += Bindable_Focused;
            bindable.Unfocused += Bindable_Unfocused;
            base.OnAttachedTo(bindable);
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            bindable.Focused -= Bindable_Focused;
            bindable.Unfocused -= Bindable_Unfocused;

            base.OnDetachingFrom(bindable);
        }

        async void Bindable_Focused(object sender, FocusEventArgs e)
        {
            Entry entryField = (Entry)sender;

            entryName = entryField.Placeholder;
            entryField.Placeholder = String.Empty;
            entryField.PlaceholderColor = Color.White;

            string labeleStr = "label" + entryField.StyleId;
            Label label = entryField.Parent.FindByName<Label>(labeleStr);

            if (entryField.Placeholder == "Required field")
            {
                await label.FadeTo(1, 600);
                entryField.PlaceholderColor = Color.Red;
            }

            await label.FadeTo(1, 600);
        }

        async void Bindable_Unfocused(object sender, FocusEventArgs e)
        {
            Entry entryField = (Entry)sender;

            if (entryField.Text.Equals("") == true)
            {
                entryField.Placeholder = entryName;

                string labelStr = "label" + entryField.StyleId;
                Label label = entryField.Parent.FindByName<Label>(labelStr);

                await label.FadeTo(0, 600);
                if (entryField.Placeholder == "Required field")
                {
                    await label.FadeTo(1, 600);
                    entryField.PlaceholderColor = Color.Red;
                }
            }
        }
    }
}
