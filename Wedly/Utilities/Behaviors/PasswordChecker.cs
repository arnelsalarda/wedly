﻿using System;
using Xamarin.Forms;

namespace Wedly.Utilities.Behaviors
{
    public class PasswordChecker : Behavior<Entry>
    {
        protected override void OnAttachedTo(Entry bindable)
        {
            bindable.Focused += Bindable_Focused;
            bindable.Unfocused += Bindable_Unfocused;
            base.OnAttachedTo(bindable);
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            bindable.Focused -= Bindable_Focused;
            bindable.Unfocused -= Bindable_Unfocused;

            base.OnDetachingFrom(bindable);
        }

        void Bindable_Focused(object sender, FocusEventArgs e)
        {
            Entry entryField = (Entry)sender;
        }

        void Bindable_Unfocused(object sender, FocusEventArgs e)
        {
            Entry entryField = (Entry)sender;
            Entry password = entryField.Parent.FindByName<Entry>("PasswordEntry");
            if (password.Text != entryField.Text)
            {
                password.Placeholder = "Password did'nt match";
                password.PlaceholderColor = Color.Red;
                entryField.Text = "";
                password.Text = "";
            }
        }
    }
}
