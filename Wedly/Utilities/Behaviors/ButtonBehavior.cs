﻿using System;
using Xamarin.Forms;

namespace Wedly.Utilities.Behaviors
{
    public class ButtonBehavior : Behavior<Button>
    {
        public static readonly BindableProperty ClickBindableProperty = BindableProperty.Create("IsClicked", typeof(bool), typeof(MainPage), false);
        public bool IsClicked
        {
            set { SetValue(ClickBindableProperty, value); }
            get { return (bool)GetValue(ClickBindableProperty); }
        }

        protected override void OnAttachedTo(Button bindable)
        {
            bindable.Clicked += Button_Clicked;
            base.OnAttachedTo(bindable);
        }
        protected override void OnDetachingFrom(Button bindable)
        {
            bindable.Clicked += Button_Clicked;
            base.OnDetachingFrom(bindable);
        }

        public void Button_Clicked(object sender, EventArgs eventArgs)
        {
            Button button = (Button)sender;
            IsClicked = true;
            Device.StartTimer(new TimeSpan(0, 0, 1, 5), () =>
            {
                IsClicked = false;
                return true;
            });
        }
    }
}
