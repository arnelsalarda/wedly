﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using Wedly.Models;
using Xamarin.Forms;

namespace Wedly.Utilities.Behaviors
{
    public class ListViewBehavior : Behavior<ListView>
    {
        Plan plan;
        ListView list;
        protected override void OnAttachedTo(ListView bindable)
        {
            list = bindable;
            CheckSource();
            base.OnAttachedTo(bindable);
        }
        protected override void OnDetachingFrom(ListView bindable)
        {
            list = bindable;
            CheckSource();
            base.OnDetachingFrom(bindable);
        }

        void CheckSource()
        {
            plan = new Plan();
            if (list.ItemsSource == null)
            {
                plan.title = "No plans yet";
                plan.rsvp = 9;
                Constants.planList.Add(plan);
            }
        }
    }
}
