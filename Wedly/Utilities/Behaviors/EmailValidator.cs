﻿using System;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace Wedly.Utilities.Behaviors
{
    public class EmailValidator : Behavior<Entry>
    {
        protected override void OnAttachedTo(Entry entry)
        {
            entry.Unfocused += OnEntryUnfocused;
            base.OnAttachedTo(entry);
        }

        protected override void OnDetachingFrom(Entry entry)
        {
            entry.Unfocused -= OnEntryUnfocused;
            base.OnDetachingFrom(entry);
        }

        void OnEntryUnfocused(object sender, FocusEventArgs e)
        {
            var entry = (Entry)sender;
            if (entry.Text.Length != 0)
            {
                if (Regex.Match(((Entry)sender).Text, @"^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$").Success)
                {
                    entry.BackgroundColor = new Color(0, 255, 0, 0.4);
                }
                else
                {
                    entry.BackgroundColor = new Color(255, 0, 0, 0.4);
                }
            }
        }
    }
}
