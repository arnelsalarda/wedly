﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Wedly.Utilities.DependecyServices
{
    public interface iImagePicker
    {
        Task<Stream> GetImageFromGalleryAsync();
    }
}
