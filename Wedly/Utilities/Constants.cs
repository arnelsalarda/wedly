﻿using System;
using System.Collections.ObjectModel;
using Wedly.Models;
using Xamarin.Forms;

namespace Wedly.Utilities
{
    public class Constants
    {
        public static string URL = "http://192.168.1.56";
        public static string VERSION = "/v1";
        public static string USER_URL = "/users";
        public static string REGISTER_URL = "/register";
        public static string SIGNIN_URL = "/sign-in";
        public static string FORGOTPASSWORD_URL = "/forgot_password";
        public static string CHANGE_PASSWORD = "/change_password";
        public static string RESET_PASSWORD = "/reset_password";
        public static string PORT = ":8000";
        public static string COMMENT_URL = "/comments";
        public static string PLAN_URL = "/plans";
        public static string ATTENDEE_URL = "/attendees";
        public static string RESPON_URL = "/respond";
        public static string ROOT_URL = URL + PORT + VERSION;
        public static int LOAD_MORE;
        public static string PAGINATION_URL = URL + PORT;

        public static ObservableCollection<Plan> planList = new ObservableCollection<Plan>();
        public static ObservableCollection<Plan> invitesList = new ObservableCollection<Plan>();
        public static readonly double NAV_GRIDLENGTH = GetGridLength();

        private static double GetGridLength()
        {
            int navHeight = 0;
            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    navHeight = 64;
                    break;
                case Device.Android:
                    navHeight = 57;
                    break;
            }
            return navHeight;
        }
    }
}
