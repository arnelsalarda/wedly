﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace Wedly.Utilities.Helpers
{
    public class ScaleHelper : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter.ToString().Contains(",") == true)
            {
                return ConvertToThicknessProperty(parameter.ToString());
            }
            else if (parameter.ToString().Contains("screenWidth") == true)
            {
                return App.ScreenWidth;
            }
            else if (parameter.ToString().Contains("screenHeight") == true)
            {
                return App.ScreenHeight;
            }
            else if (parameter.ToString().Contains("-scaleWidth") == true)
            {
                var par = parameter.ToString();
                return (double.Parse(par.Remove(par.IndexOf('-')))) * (App.ScreenWidth / 320);
            }
            else if (parameter.ToString().Contains("-scaleHeight") == true)
            {
                var par = parameter.ToString();
                return (double.Parse(par.Remove(par.IndexOf('-')))) * (App.ScreenHeight / 568);
            }
            else if(parameter.ToString().Contains("navHeight") == true)
            {
                var par = Constants.NAV_GRIDLENGTH.ToString();
                return (double.Parse(par)) * (App.ScreenHeight / 568);
            }

            return (double.Parse(parameter.ToString()) * App.ScreenScale);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private object ConvertToThicknessProperty(string value)
        {
            double l, t, r, b;
            string[] thickness = value.Split(',');
            switch (thickness.Length)
            {
                case 1:
                    if (double.TryParse(thickness[0], NumberStyles.Number, CultureInfo.InvariantCulture, out l))
                        return new Thickness(l * App.ScreenScale);
                    break;
                case 2:
                    if (double.TryParse(thickness[0], NumberStyles.Number, CultureInfo.InvariantCulture, out l) && double.TryParse(thickness[1], NumberStyles.Number, CultureInfo.InvariantCulture, out t))
                        return new Thickness(l * App.ScreenScale, t * App.ScreenScale);
                    break;
                case 4:
                    if (double.TryParse(thickness[0], NumberStyles.Number, CultureInfo.InvariantCulture, out l) && double.TryParse(thickness[1], NumberStyles.Number, CultureInfo.InvariantCulture, out t) &&
                        double.TryParse(thickness[2], NumberStyles.Number, CultureInfo.InvariantCulture, out r) && double.TryParse(thickness[3], NumberStyles.Number, CultureInfo.InvariantCulture, out b))
                        return new Thickness(l * App.ScreenScale, t * App.ScreenScale, r * App.ScreenScale, b * App.ScreenScale);
                    break;
            }

            throw new InvalidOperationException("Cannot convert thickness");
        }
    }
}
