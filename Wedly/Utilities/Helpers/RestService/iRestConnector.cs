﻿using System;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace Wedly.Utilities.Helpers.RestService
{
    public interface iRestConnector
    {
        void ReceiveJSONData(JObject dataDictionary, CancellationToken ct);
        void ReceiveTimeoutError(int error, string errorStr);
    }
}
