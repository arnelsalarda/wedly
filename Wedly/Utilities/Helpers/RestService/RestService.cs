﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;
using System.Diagnostics;
using System.Threading;

namespace Wedly.Utilities.Helpers.RestService
{
    public class RestService
    {
        HttpClient client;
        NetworkHelper.NetworkHelper networkHelper = NetworkHelper.NetworkHelper.GetInstance;

        WeakReference<iRestConnector> _restServiceDelegate;
        public iRestConnector RestServiceDelegate
        {
            get
            {
                iRestConnector restServiceDelegate;
                return _restServiceDelegate.TryGetTarget(out restServiceDelegate) ? restServiceDelegate : null;
            }

            set
            {
                _restServiceDelegate = new WeakReference<iRestConnector>(value);
            }
        }

        public RestService()
        {
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
        }

        public async Task CreateData<Classname>(string url, object item, CancellationToken ct)
        {
            if (networkHelper.HasInternet())
            {
                if (await networkHelper.IsHostReachable())
                {
                    try
                    {
                        var uri = new Uri(url);
                        var json = JsonConvert.SerializeObject(item);
                        var content = new StringContent(json, Encoding.UTF8, "application/json");

                        HttpResponseMessage response = await client.PostAsync(uri, content, ct);
                        if (response.IsSuccessStatusCode)
                        {
                            var responseMsg = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                            RestServiceDelegate?.ReceiveJSONData(JObject.Parse(responseMsg), ct);
                        }
                        else
                        {
                            RestServiceDelegate?.ReceiveTimeoutError(1, "Host not reachable");
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                        RestServiceDelegate?.ReceiveTimeoutError(1, e.Message);
                    }
                }
                else
                {
                    RestServiceDelegate?.ReceiveTimeoutError(1, "Host not reachable");
                }
            }
            else
            {
                RestServiceDelegate?.ReceiveTimeoutError(1, "No internet connection");
            }
        }

        public async Task DeleteDataAsync<Classname>(string url, CancellationToken ct)
        {
            var uri = new Uri(url);

            try
            {
                var response = await client.DeleteAsync(uri);

                if (response.IsSuccessStatusCode)
                {
                    var responseMsg = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    RestServiceDelegate?.ReceiveJSONData(JObject.Parse(responseMsg), ct);
                }
                else
                {
                    Console.WriteLine("Student does not exist!!");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {0}", ex.Message);
            }
        }

        public async Task GetData<Classname>(string url, CancellationToken ct)
        {
            if (networkHelper.HasInternet())
            {
                if (await networkHelper.IsHostReachable())
                {
                    try
                    {
                        var uri = new Uri(url);
                        //var json = JsonConvert.SerializeObject(item);
                        //var content = new StringContent(json, Encoding.UTF8, "application/json");

                        HttpResponseMessage response = await client.GetAsync(uri, ct);
                        if (response.IsSuccessStatusCode)
                        {
                            //ct.ThrowIfCancellationRequested();
                            var responseMsg = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                            RestServiceDelegate?.ReceiveJSONData(JObject.Parse(responseMsg), ct);
                        }
                        else
                        {
                            RestServiceDelegate?.ReceiveTimeoutError(1, "Request failed");
                        }
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine(e.Message);
                        RestServiceDelegate?.ReceiveTimeoutError(1, e.Message);
                    }
                }
                else
                {
                    RestServiceDelegate?.ReceiveTimeoutError(1, "Host not reachable");
                }
            }
            else
            {
                RestServiceDelegate?.ReceiveTimeoutError(1, "No internet connection");
            }
        }

        public async Task UpdateDataAsync<Classname>(string url, object item, CancellationToken ct)
        {
            if (networkHelper.HasInternet())
            {
                if (await networkHelper.IsHostReachable())
                {
                    try
                    {
                        var uri = new Uri(url);
                        var json = JsonConvert.SerializeObject(item);
                        var content = new StringContent(json, Encoding.UTF8, "application/json");

                        HttpResponseMessage response = await client.PutAsync(uri, content, ct);
                        if (response.IsSuccessStatusCode)
                        {
                            var responseMsg = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                            RestServiceDelegate?.ReceiveJSONData(JObject.Parse(responseMsg), ct);
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(@"ERROR {0}", ex.Message);
                        RestServiceDelegate?.ReceiveTimeoutError(1, ex.Message);
                    }
                }
                else
                {
                    RestServiceDelegate?.ReceiveTimeoutError(1, "Host not reachable");
                }
            }
            else
            {
                RestServiceDelegate?.ReceiveTimeoutError(1, "No internet connection");
            }
        }

        public async Task GetProfile<Classname>(string url, CancellationToken ct)
        {
            var uri = new Uri(url);
            var response = await client.GetAsync(uri);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var json = JObject.Parse(content);
                Console.WriteLine(json);
            }
        }

        //public async Task MultiPartDataContentAsync(string url, string key, CancellationToken ct, object item = null, System.IO.Stream image = null)
        //{
        //    if (networkHelper.HasInternet())
        //    { 
        //        if (await networkHelper.IsHostReachable() == true)
        //        {
        //            var dictionary = JsonConvert.SerializeObject(item);
        //            var uri = new Uri(url);
        //            var json = JObject.Parse(dictionary)[key];
        //            var tokenJson = JObject.Parse(dictionary)["token"];
        //            var multipartFormData = new MultipartFormDataContent();

        //            foreach (var obj in json)
        //            {
        //                string[] keys = obj.Path.Split('.');

        //                if (keys[1].ToString() == "avatar" && image != null)
        //                {
        //                    string name = "\"" + keys[0] + "[" + keys[1] + "]" + "\"";
        //                    //StreamContent content = new StreamContent(file.GetStream());
        //                    StreamContent content = new StreamContent(image);
        //                    content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data") { FileName = "\"upload-image.jpeg\"", Name = name };
        //                    multipartFormData.Add(content);
        //                }
        //                else
        //                {
        //                    if (!string.IsNullOrEmpty(obj.First.ToString()))
        //                    {
        //                        string keyName = "\"" + keys[0] + "[" + keys[1] + "]" + "\"";
        //                        StringContent content = new StringContent(obj.First.ToString(), System.Text.Encoding.UTF8);
        //                        content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data") { Name = keyName };
        //                        multipartFormData.Add(content);
        //                    }
        //                }
        //            }

        //            //string tokenName = "\"token\"";
        //            //StringContent contentTokens = new StringContent(tokenJson.ToString(), System.Text.Encoding.UTF8);
        //            //contentTokens.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data") { Name = tokenName };
        //            //multipartFormData.Add(contentTokens);

        //            try
        //            {
        //                HttpResponseMessage response = await client.PostAsync(uri, multipartFormData);
        //                //await RequestAsync(response);
        //                if (response.IsSuccessStatusCode)
        //                {
        //                    var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
        //                    RestServiceDelegate?.ReceiveJSONData(JObject.Parse(result), ct);
        //                }
        //                else
        //                {
        //                    //300, 404, 500
        //                    RestServiceDelegate?.ReceiveTimeoutError(3, response.StatusCode.ToString());
        //                }
        //            }
        //            catch (HttpRequestException ex)
        //            {
        //                //DependencyService.Get<iConsole>().DisplayText("Multi Part Data Error" + ex);
        //            }
        //        }
        //        else
        //        {
        //            RestServiceDelegate?.ReceiveTimeoutError(2, "The URL host for Idle cannot be reached and seems to be unavailable. Please try again later!");
        //        }
        //    }
        //    else
        //    {
        //        RestServiceDelegate?.ReceiveTimeoutError(1, "Please check your internet connection, and try again!");
        //    }
        //}

        public async Task MultiPartDataContentAsync(string url, List<string> arrayKeys, CancellationToken ct, bool isEditing, object item = null, System.IO.Stream[] image = null, List<string> extraKeys = null)
        {
            if (networkHelper.HasInternet())
            {
                if (await networkHelper.IsHostReachable() == true)
                {
                    var dictionary = JsonConvert.SerializeObject(item);
                    var uri = new Uri(url);
                    //var tokenJson = JObject.Parse(dictionary)["token"];
                    var multipartFormData = new MultipartFormDataContent();

                    int x = 0;

                    foreach (var xkey in arrayKeys)
                    {
                        var json = JObject.Parse(dictionary)[xkey];
                        foreach (var obj in json)
                        {
                            string[] keys = obj.Path.Split('.');
                            try
                            {
                                if(keys.Length < 2)
                                {
                                    keys[0] = keys[0].Remove(keys[0].IndexOf('[') + 1) + "]";
                                    if (!string.IsNullOrEmpty(obj.First.ToString()))
                                    {
                                        foreach (var child in obj)
                                        {
                                            string[] childKeys = child.Path.Split('.');
                                            childKeys[0] = childKeys[0].Remove(childKeys[0].IndexOf('[') + 1) + "]";
                                            if (childKeys[1] == "id" || childKeys[1] == "role")
                                            {
                                                string name;
                                                if(childKeys[1] == "id")
                                                {
                                                    name = "\"" + childKeys[0] + "[user_id]" + "\"";
                                                }
                                                else
                                                {
                                                    name = "\"" + childKeys[0] + "[" + childKeys[1] + "]" + "\"";
                                                }
                                                StringContent content = new StringContent(child.First.ToString(), System.Text.Encoding.UTF8);
                                                content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data") { Name = name };
                                                multipartFormData.Add(content);
                                            }
                                        }
                                    }
                                }
                                else if ((keys[1].Contains("_photo") || keys[1].Contains("banner") || keys[1].Contains("image") || keys[1].Contains("_image") || keys[1].Contains("avatar")) && image[x] != null)
                                {
                                    string name = "\"" + keys[0] + "[" + keys[1] + "]" + "\"";
                                    //StreamContent content = new StreamContent(file.GetStream());
                                    StreamContent content = new StreamContent(image[x]);
                                    x++;
                                    content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data") { FileName = "\"" + keys[1] + ".jpeg\"", Name = name };
                                    Debug.WriteLine(name);
                                    multipartFormData.Add(content);
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(obj.First.ToString()))
                                    {
                                        string keyName = "\"" + keys[0] + "[" + keys[1] + "]" + "\"";
                                        StringContent content = new StringContent(obj.First.ToString(), System.Text.Encoding.UTF8);
                                        content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data") { Name = keyName };
                                        multipartFormData.Add(content);
                                    }
                                }
                            }
                            catch (Exception e) { }
                        }
                    }

                    try
                    {
                        foreach (var key in extraKeys)
                        {
                            var tokenJson = JObject.Parse(dictionary)[key];
                            string tokenName = "\"" + key + "\"";
                            StringContent contentTokens = new StringContent(tokenJson.ToString(), System.Text.Encoding.UTF8);
                            contentTokens.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data") { Name = tokenName };
                            multipartFormData.Add(contentTokens);
                        }
                    }
                    catch (Exception e) { }

                    try
                    {
                        HttpResponseMessage response;
                        if (isEditing == false)
                        {
                            response = await client.PostAsync(uri, multipartFormData);
                        }
                        else
                        {
                            response = await client.PutAsync(uri, multipartFormData);

                        }
                        //await RequestAsync(response);
                        if (response.IsSuccessStatusCode)
                        {
                            var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                            RestServiceDelegate?.ReceiveJSONData(JObject.Parse(result), ct);
                        }
                        else
                        {
                            //300, 404, 500
                            RestServiceDelegate?.ReceiveTimeoutError(1, response.StatusCode.ToString());
                        }
                    }
                    catch (HttpRequestException ex)
                    {
                        //DependencyService.Get<iConsole>().DisplayText("Multi Part Data Error" + ex);
                        Debug.WriteLine(ex.Message);
                    }
                }
                else
                {
                    RestServiceDelegate?.ReceiveTimeoutError(2, "The URL host for Idle cannot be reached and seems to be unavailable. Please try again later!");
                }
            }
            else
            {
                RestServiceDelegate?.ReceiveTimeoutError(3, "Please check your internet connection, and try again!");
            }
        }
    }
}
