﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;

namespace Wedly.Utilities.Helpers.RestService
{
    public interface iRestService
    {
        Task GetData<Classname>(string url, CancellationToken ct);
        Task CreateData<Classname>(string url, object item, CancellationToken ct);
        Task UpdateDataAsync<Classname>(string url, object item, CancellationToken ct);
        Task DeleteDataAsync<Classname>(string url, CancellationToken ct);
        Task GetProfile<Classname>(string url, CancellationToken ct);
        Task MultiPartDataContentAsync(string url, List<string> arrayKeys, CancellationToken ct, bool isEditing, object item = null, System.IO.Stream image = null, List<string> extraKeys = null);
    }
}
