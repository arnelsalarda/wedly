﻿using System;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace Wedly.Utilities.Helpers.FileReader
{
    public interface iFileConnector
    {
        void ReceiveJSONData(JObject dataDictionary, CancellationToken ct);
        void ReceiveTimeoutError(int error, string errorStr);
    }
}
