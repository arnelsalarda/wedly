﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Wedly.Utilities.Helpers.FileReader
{
    public interface iFileReader
    {
        Task ReadFile(string fileName, bool isEmbed, CancellationToken ct);
    }
}
