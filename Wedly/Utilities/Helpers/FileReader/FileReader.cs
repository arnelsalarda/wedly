﻿using System;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Wedly.Utilities.Helpers.FileReader
{
    public class FileReader : iFileReader
    {
        NetworkHelper.NetworkHelper networkHelper = NetworkHelper.NetworkHelper.GetInstance;

        WeakReference<iFileConnector> _fileReaderDelegate;
        public iFileConnector FileReaderDelegate
        {
            get
            {
                iFileConnector fileReaderDelegate;
                return _fileReaderDelegate.TryGetTarget(out fileReaderDelegate) ? fileReaderDelegate : null;
            }

            set
            {
                _fileReaderDelegate = new WeakReference<iFileConnector>(value);
            }
        }

        public async Task ReadFile(string fileName, bool isEmbed, CancellationToken ct)
        {
            if (networkHelper.HasInternet())
            {
                if (isEmbed)
                {
                    var assembly = typeof(FileReader).GetTypeInfo().Assembly;

                    Stream stream = assembly.GetManifestResourceStream("Wedly.Utilities.Files." + fileName);

                    using (var reader = new StreamReader(stream))
                    {
                        var json = reader.ReadToEnd();

                        FileReaderDelegate?.ReceiveJSONData(JObject.Parse(json), ct);
                    }
                }
            }
            else
            {
                FileReaderDelegate?.ReceiveTimeoutError(1, "Failed");
            }
        }
    }
}
