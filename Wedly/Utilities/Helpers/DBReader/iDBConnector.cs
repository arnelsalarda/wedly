﻿using System;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace Wedly.Utilities.Helpers.DBReader
{
    public interface iDBConnector
    {
        void ReceiveJSONData(JObject dataDictionary, CancellationToken ct);
    }
}
