﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Wedly.Utilities.Helpers.DBReader
{
    public interface iDBReader
    {
        void CreateTable<ClassName>() where ClassName : new();
        Task FetchData<ClassName>(CancellationToken ct) where ClassName : new();
        Task SaveItemAsync<ClassName>(object dataDictionary, string tableName) where ClassName : new();
    }
}
