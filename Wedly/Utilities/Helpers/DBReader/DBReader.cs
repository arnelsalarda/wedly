﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SQLite;

namespace Wedly.Utilities.Helpers.DBReader
{
    public class DBReader : iDBReader
    {
        readonly SQLiteAsyncConnection database;

        public static DBReader dbReader;
        public static DBReader GetInstance
        {
            get
            {
                if (dbReader == null)
                {
                    dbReader = new DBReader(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "TodoSQLite.db3"));
                }

                return dbReader;
            }
        }

        WeakReference<iDBConnector> _dbReaderDelegate;
        public iDBConnector DBReaderDelegate
        {
            get
            {
                iDBConnector dbReaderDelegate;
                return _dbReaderDelegate.TryGetTarget(out dbReaderDelegate) ? dbReaderDelegate : null;
            }

            set
            {
                _dbReaderDelegate = new WeakReference<iDBConnector>(value);
            }
        }

        public DBReader(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
        }

        public void CreateTable<ClassName>() where ClassName : new()
        {
            database.CreateTableAsync<ClassName>().Wait();
        }

        public async Task FetchData<ClassName>(CancellationToken ct) where ClassName : new()
        {
            await database.CreateTableAsync<ClassName>();

            var list = await database.Table<ClassName>().ToListAsync();

            var parseData = JsonConvert.SerializeObject(new { list });
            DBReaderDelegate?.ReceiveJSONData(JObject.Parse(parseData), ct);
        }

        public async Task SaveItemAsync<ClassName>(object dataDictionary, string tableName) where ClassName : new()
        {
            var obj = JObject.Parse(JsonConvert.SerializeObject(new { dataDictionary }));

            var data = database.QueryAsync<ClassName>("SELECT * FROM [" + tableName + "] WHERE [id] = " + obj["dataDictionary"]["id"]);

            if (data.Result.Count == 0)
            {
                await database.InsertAsync(dataDictionary);
            }
            else
            {
                await database.UpdateAsync(dataDictionary);
            }
        }
    }
}
