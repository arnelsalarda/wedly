﻿using System;
using System.Threading.Tasks;

namespace Wedly.Utilities.Helpers.NetworkHelper
{
    public interface iNetworkHelper
    {
        bool HasInternet();
        Task<bool> IsHostReachable();
    }
}
