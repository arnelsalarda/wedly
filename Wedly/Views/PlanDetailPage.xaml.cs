﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Wedly.Models;
using Wedly.Utilities;
using Wedly.Utilities.DependecyServices;
using Wedly.Utilities.Helpers.FileReader;
using Wedly.Utilities.Helpers.RestService;
using Xamarin.Forms;

namespace Wedly.Views
{
    public partial class PlanDetailPage : RootViewPage, iFileConnector, iRestConnector
    {
        ObservableCollection<Comment> newComments;
        ObservableCollection<Comment> comments;
        ObservableCollection<User> newAttendees;
        ObservableCollection<User> attendees;
        CancellationTokenSource ct = new CancellationTokenSource();
        RestService restService = new RestService();
        FileReader fileReader = new FileReader();
        Stream[] streams = new MemoryStream[1];
        bool didRespond = false;
        bool isDBCalled = false;
        Comment comment;
        string userRole;
        int tempOffSet;
        Plan plan;
        int rsvp;

        public PlanDetailPage(Plan plan)
        {
            BindingContext = plan;
            InitializeComponent();
            this.plan = plan;
#if DEBUG
            fileReader.FileReaderDelegate = this;
#else
            restService.RestServiceDelegate = this;
#endif
            GetComments();
            SetNavigation();
            //CommentsList.ItemsSource = plan.comments;
            Console.WriteLine(plan.id + " identification");
        }

        void SetNavigation()
        {
            this.LeftIcon = "Close";
            this.LeftButtonCommand = new Command((x) => Navigation.PopModalAsync());
            if (plan.is_owner == 1)
            {
                this.RightIcon2 = "Edit";
                this.RightButton2Command = new Command((x) => Navigation.PushModalAsync(new CreatePlanPage(plan)));
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            BindingContext = null;
            BindingContext = plan;
        }

        void GetComments()
        {
            Task.Run(async () =>
            {
#if DEBUG
                await fileReader.ReadFile("Attendees.json", true, ct.Token);
#else
                string url = Constants.ROOT_URL + Constants.PLAN_URL + "/" + plan.id + Constants.COMMENT_URL + "?token=" + Application.Current.Properties["user_token"];
                string url2 = Constants.ROOT_URL + Constants.PLAN_URL + "/" + plan.id + Constants.ATTENDEE_URL + "?token=" + Application.Current.Properties["user_token"];
                Debug.WriteLine(url);
                await restService.GetData<Comment>(url, ct.Token);
                await restService.GetData<User>(url2, ct.Token);
#endif
            });
        }

        void GoingButton_Clicked(object sender, EventArgs e)
        {
#if DEBUG
            if (plan.rsvp == 4)
            {
                Constants.invitesList.Remove(plan);
                plan.rsvp = 1;
                Constants.planList.Insert(0, plan);
                Refresh();
            }
            else
            {
                int index = Constants.planList.IndexOf(plan);
                Constants.planList.Remove(plan);
                plan.rsvp = 1;
                Constants.planList.Insert(index, plan);
                Refresh();
            }
#else
            Task.Run(async () =>
            {
                string url = Constants.ROOT_URL + Constants.PLAN_URL + "/" + plan.id + Constants.ATTENDEE_URL + Constants.RESPON_URL;
                didRespond = true;
                rsvp = 1;
                await restService.UpdateDataAsync<User>(url, new { attendee = new { role = userRole, rsvp = 1}, token = Application.Current.Properties["user_token"] }, ct.Token);
            });
#endif
        }

        void MaybeButton_Clicked(object sender, EventArgs e)
        {
#if DEBUG
            if (plan.rsvp == 4)
            {
                Constants.invitesList.Remove(plan);
                plan.rsvp = 2;
                Constants.planList.Insert(0, plan);
                Refresh();
            }
            else
            {
                int index = Constants.planList.IndexOf(plan);
                Constants.planList.Remove(plan);
                plan.rsvp = 2;
                Constants.planList.Insert(index, plan);
                Refresh();
            }
#else
            Task.Run(async () =>
            {
                string url = Constants.ROOT_URL + Constants.PLAN_URL + "/" + plan.id + Constants.ATTENDEE_URL + Constants.RESPON_URL;
                didRespond = true;
                rsvp = 2;
                await restService.UpdateDataAsync<User>(url, new { attendee = new { role = userRole, rsvp = 2 }, token = Application.Current.Properties["user_token"] }, ct.Token);
            });
#endif
        }

        void NotGoingButton_Clicked(object sender, EventArgs e)
        {
#if DEBUG
            if (plan.rsvp == 4)
            {
                Constants.invitesList.Remove(plan);
                plan.rsvp = 3;
                Constants.planList.Insert(0, plan);
                Refresh();
            }
            else
            {
                int index = Constants.planList.IndexOf(plan);
                Constants.planList.Remove(plan);
                plan.rsvp = 3;
                Constants.planList.Insert(index, plan);
                Refresh();
            }
#else
            Task.Run(async () =>
            {
                string url = Constants.ROOT_URL + Constants.PLAN_URL + "/" + plan.id + Constants.ATTENDEE_URL + Constants.RESPON_URL;
                didRespond = true;
                rsvp = 3;
                await restService.UpdateDataAsync<User>(url, new { attendee = new { role = userRole, rsvp = 3 }, token = Application.Current.Properties["user_token"] }, ct.Token);
            });
#endif
        }

        void Refresh()
        {
            this.BindingContext = null;
            this.BindingContext = plan;
        }

        void OnImage_Clicked(object sender, EventArgs e)
        {
            PopUpDialog.ShowDialog();
        }

        void PostComment_Clicked(object sender, System.EventArgs e)
        {
            Task.Run(async () =>
            {
                if (CommentEditor.Text != "")
                {
#if DEBUG
                    comment = new Comment();
                    comment.avatar = Application.Current.Properties["user_avatar"].ToString();
                    comment.name = Application.Current.Properties["user_name"].ToString();
                    comment.image = "No Image";
                    comment.content = CommentEditor.Text;
                    CommentEditor.Text = "";
                    plan.comments.Insert(0, comment);
#else
                string url = Constants.ROOT_URL + Constants.PLAN_URL + "/" + plan.id + Constants.COMMENT_URL;
                LoadingIndicator.IsVisible = true;
                await restService.CreateData<Comment>(url, new { comment = new { content = CommentEditor.Text }, token =  Application.Current.Properties["user_token"]}, ct.Token);
#endif
                }
            });
        }

        async void OnGalleryImage_Clicked(object sender, EventArgs e)
        {
            comment = new Comment();
            Stream chosenImage = await DependencyService.Get<iImagePicker>().GetImageFromGalleryAsync();
            if (chosenImage != null)
            {
#if DEBUG
                comment.content = "";
                //comment.image = chosenImage;
                comment.avatar = Application.Current.Properties["user_avatar"].ToString();
                comment.name = Application.Current.Properties["user_name"].ToString();
                plan.comments.Insert(0, comment);
#else
                List<string> xkeys = new List<string>() { "token" };
                List<string> keys = new List<string>() { "comment" };
                if (chosenImage != null)
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        chosenImage.CopyTo(ms);
                        byte[] chosen = ms.ToArray();
                        streams[0] = new MemoryStream(chosen);
                    }
                }
                string url = Constants.ROOT_URL + Constants.PLAN_URL + "/" + plan.id + Constants.COMMENT_URL;
                LoadingIndicator.IsVisible = true;
                await restService.MultiPartDataContentAsync(url, keys, ct.Token, false, new { comment = new { image = "" }, token = Application.Current.Properties["user_token"] }, streams, xkeys);
#endif
            }
        }

        async void DeletePlanButton_Clicked(object sender, EventArgs e)
        {
            var answer = await DisplayAlert("Delete Plan", "Confirm plan deletion.", "Delete", "Cancel");
            Console.WriteLine("Answer: " + answer); // writes true or false to the console
            if (answer == true)
            {
                Task.Run(async () =>
                {
                    LoadingIndicator.IsVisible = true;
                    string url = Constants.ROOT_URL + Constants.PLAN_URL + "/" + plan.id + "?token=" + Application.Current.Properties["user_token"];
                    await restService.DeleteDataAsync<Plan>(url, ct.Token);
                });
            }
        }

        public void ReceiveJSONData(JObject dataDictionary, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if ((int.Parse(dataDictionary["status"].ToString())) == 200)
                {
                    try
                    {
                        var paginations = JsonConvert.DeserializeObject<Pagination>((dataDictionary["pagination"].ToString()));
                        if (paginations.off_set == 10)
                        {
                            try
                            {
                                attendees = JsonConvert.DeserializeObject<ObservableCollection<User>>(dataDictionary[isDBCalled == true ? "list" : "users"].ToString());
                                AttendeesList.ItemsSource = attendees;
                                plan.attendee = attendees;
                                TotalAttendees.Text = paginations.total_attendees.ToString();
                                foreach (var user in attendees)
                                {
                                    if (user.name == Application.Current.Properties["user_name"].ToString())
                                    {
                                        userRole = user.role;
                                    }
                                }
                            }
                            catch (Exception e) { Debug.WriteLine(e.Message); }
                            try
                            {
                                comments = JsonConvert.DeserializeObject<ObservableCollection<Comment>>(dataDictionary[isDBCalled == true ? "list" : "comments"].ToString());
                                CommentsList.ItemsSource = comments;
                            }catch (Exception e) { Debug.WriteLine(e.Message); }
                        }
                        else if(paginations.off_set > 10)
                        {
                            try
                            {
                                newAttendees = JsonConvert.DeserializeObject<ObservableCollection<User>>(dataDictionary[isDBCalled == true ? "list" : "users"].ToString());
                                attendees = new ObservableCollection<User>(attendees.Concat(newAttendees));
                            }
                            catch (Exception e) { Debug.WriteLine(e.Message); }
                            try
                            {
                                newComments = JsonConvert.DeserializeObject<ObservableCollection<Comment>>(dataDictionary[isDBCalled == true ? "list" : "comments"].ToString());
                                comments = new ObservableCollection<Comment>(comments.Concat(newComments));
                            }
                            catch (Exception e) { Debug.WriteLine(e.Message); }
                        }
                        if (didRespond == true)
                        {
                            plan.rsvp = rsvp;
                            Refresh();
                            didRespond = false;
                        }
                        tempOffSet = paginations.off_set;
                        Constants.PAGINATION_URL = paginations.url;
                        Constants.LOAD_MORE = paginations.load_more;
                    }catch (Exception e) { Debug.WriteLine(e.Message); }
                    try
                    {
                        LoadingIndicator.IsVisible = false;
                        var userComment = JsonConvert.DeserializeObject<Comment>(dataDictionary[isDBCalled == true ? "list" : "comment"].ToString());
                        CommentEditor.Text = "";
                        comments.Insert(0, userComment);
                    }
                    catch (Exception e) { Debug.WriteLine(e.Message); }
                    try
                    {
                        if ((dataDictionary["message"].ToString()) != "")
                        {
                            LoadingIndicator.IsVisible = false;
                            Navigation.PopModalAsync();
                            DependencyService.Get<iToast>().ShowMessage("Plan succesfully deleted");
                        }
                    } catch(Exception e) {}
                }
            });
        }

        public void ReceiveTimeoutError(int error, string errorStr)
        {
            DependencyService.Get<iToast>().ShowMessage(errorStr);
        }
    }
}
