﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Wedly.Views
{
    public partial class LandingPage : MasterDetailPage
    {
        public LandingPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ((MasterDetailPage)App.Current.MainPage).Detail = new NavigationPage(new PlansPage());
            ((MasterDetailPage)App.Current.MainPage).IsPresented = false;
        }
    }
}
