﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Wedly.Models;
using Wedly.Utilities;
using Wedly.Utilities.DependecyServices;
using Wedly.Utilities.Helpers.DBReader;
using Wedly.Utilities.Helpers.FileReader;
using Wedly.Utilities.Helpers.RestService;
using Xamarin.Forms;

namespace Wedly.Views
{
    public partial class PlansPage : RootViewPage, iFileConnector, iRestConnector
    {
        CancellationTokenSource ct = new CancellationTokenSource();
        RestService restService = new RestService();
        FileReader fileReader = new FileReader();
        bool didClickedInvitesButton = false;
        ObservableCollection<Plan> newPlans;
        ObservableCollection<Plan> plans;
        bool isDBCalled = false;
        int tempOffSet;

        public PlansPage()
        {
            InitializeComponent();
            SetNavigation();
            GetData();
#if DEBUG
            fileReader.FileReaderDelegate = this;
#else
            restService.RestServiceDelegate = this;
#endif
        }

        void SetNavigation()
        {
            this.PageTitle = "Plans";
            this.LeftIcon = "Menu";
            this.RightIcon1 = "Envelope";
            this.RightIcon2 = "ThreeDots";
            this.RightButton1Visibility = true;
            this.TitleFontColor = Color.FromHex("1E90FF");
            this.RightButton1Command = new Command((x) => ShowInvites());
            this.RightButton2Command = new Command((x) => EllipsisCommand());
            this.LeftButtonCommand = new Command((x) => ((MasterDetailPage)Application.Current.MainPage).IsPresented = true);
        }

        void ShowInvites()
        {
            Task.Run(async () =>
            {
                PopUpDialog.ShowDialog();
                didClickedInvitesButton = true;
                string url = Constants.ROOT_URL + Constants.PLAN_URL + "?token=" + Application.Current.Properties["user_token"] + "&filter=0";
                await restService.GetData<Plan>(url, ct.Token);
            });
        }

        async void EllipsisCommand()
        {
                var action = await DisplayActionSheet("Options", "Cancel", null, "Option 1", "Option 2", "Option 3");

                if (string.IsNullOrEmpty(action) || action == "Cancel")
                    return;

                if (action == "Option 1")
                {
                    Console.WriteLine("if");
                }
                else if (action == "Option 2")
                {
                    Console.WriteLine("else if");
                }
                else if (action == "Option 3")
                {
                    Console.WriteLine("else");
                }
        }

        void OnImage_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new CreatePlanPage());
        }

        void Plan_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Plan planList = (Plan)PlansList.SelectedItem;
            Navigation.PushModalAsync(new PlanDetailPage(planList));
            PlansList.SelectedItem = null;
        }

        void Invite_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Plan invitesList = (Plan)InvitesList.SelectedItem;
            Navigation.PushModalAsync(new PlanDetailPage(invitesList));
            PopUpDialog.HideDialog();
            InvitesList.SelectedItem = null;
        }

        async void Plans_ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            if (Constants.LOAD_MORE == 1 && plans.IndexOf((Plan)e.Item) == tempOffSet - 1)
            {
                await restService.GetData<Plan>(Constants.PAGINATION_URL, ct.Token);
            }
        }

        void GetData()
        {
            Task.Run(async () =>
            {
#if DEBUG
                await fileReader.ReadFile("WeddingPlans.json", true, ct.Token);
#else
                string url = Constants.ROOT_URL + Constants.PLAN_URL + "?token=" + Application.Current.Properties["user_token"];// + "&filter=all";
                await restService.GetData<Plan>(url , ct.Token);
#endif
            });
        }

        public void ReceiveJSONData(JObject dataDictionary, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if ((int.Parse(dataDictionary["status"].ToString())) == 200)
                {
                    var paginations = JsonConvert.DeserializeObject<Pagination>((dataDictionary["pagination"].ToString()));
                    if (paginations.off_set == 10)
                    {
                        plans = JsonConvert.DeserializeObject<ObservableCollection<Plan>>(dataDictionary[isDBCalled == true ? "list" : "plans"].ToString());
                    }
                    else if(paginations.off_set > 10)
                    {
                        newPlans = JsonConvert.DeserializeObject<ObservableCollection<Plan>>(dataDictionary[isDBCalled == true ? "list" : "plans"].ToString());
                        plans = new ObservableCollection<Plan>(plans.Concat(newPlans));
                    }
                    Console.WriteLine(dataDictionary);
                    if (didClickedInvitesButton == true)
                    {
                        InvitesList.ItemsSource = plans;
                        didClickedInvitesButton = false;
                    }
                    else
                    {
                        PlansList.ItemsSource = plans;
                        Debug.WriteLine(dataDictionary);
                    }
                    tempOffSet = paginations.off_set;
                    Constants.PAGINATION_URL = paginations.url;
                    Constants.LOAD_MORE = paginations.load_more;
                }
            });
        }

        public void ReceiveTimeoutError(int error, string errorStr)
        {
            DependencyService.Get<iToast>().ShowMessage(errorStr);
        }
    }
}
