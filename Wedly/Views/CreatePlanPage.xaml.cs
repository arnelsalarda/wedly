﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Wedly.Models;
using Wedly.Utilities;
using Wedly.Utilities.DependecyServices;
using Wedly.Utilities.Helpers.FileReader;
using Wedly.Utilities.Helpers.RestService;
using Xamarin.Forms;

namespace Wedly.Views
{
    public partial class CreatePlanPage : RootViewPage, iFileConnector, iRestConnector
    {
        //ObservableCollection<Comment> comment = new ObservableCollection<Comment>();
        ObservableCollection<User> attendees = new ObservableCollection<User>();
        ObservableCollection<User> searchUser = new ObservableCollection<User>();
        CancellationTokenSource ct = new CancellationTokenSource();
        RestService restService = new RestService();
        FileReader fileReader = new FileReader();
        Stream[] streams = new MemoryStream[3];
        bool isDBCalled = false;
        bool isEditing = false;
        Plan plan = new Plan();
        User user;

        public CreatePlanPage()
        {
            InitializeComponent();
            SetNavigation();
#if DEBUG
            fileReader.FileReaderDelegate = this;
#else
            restService.RestServiceDelegate = this;
#endif
            GetData();
            InvitesList.ItemsSource = attendees;
        }

        public CreatePlanPage(Plan editPlan)
        {
            BindingContext = editPlan;
            InitializeComponent();
            SetNavigation();
#if DEBUG
            fileReader.FileReaderDelegate = this;
#else
            restService.RestServiceDelegate = this;
#endif
            GetData();
            plan = editPlan;
            //attendees = new ObservableCollection<User>(attendees.Concat(plan.attendee));
            attendees = plan.attendee;
            InvitesList.ItemsSource = attendees;
            //SearchList.ItemsSource = new ObservableCollection<User>();
            isEditing = true;
        }

        void GetData()
        {
#if DEBUG
            Task.Run(async () =>
            {
                await fileReader.ReadFile("Users.json", true, ct.Token);
            });
#endif
        }
        
        async void OnImage_Clicked(object sender, EventArgs e)
        {
            Image image = (Image)sender;
            Stream chosenImage = await DependencyService.Get<iImagePicker>().GetImageFromGalleryAsync();
            if (chosenImage != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    chosenImage.CopyTo(ms);
                    byte[] chosen = ms.ToArray();

                    if (image.StyleId == "0")
                    {
                        streams[0] = new MemoryStream(chosen);
                        BannerImage.Source = ImageSource.FromStream(() => new MemoryStream(chosen));
                    }
                    else if (image.StyleId == "1")
                    {
                        streams[1] = new MemoryStream(chosen);
                        GroomImage.Source = ImageSource.FromStream(() => new MemoryStream(chosen));
                    }
                    else
                    {
                        streams[2] = new MemoryStream(chosen);
                        BrideImage.Source = ImageSource.FromStream(() => new MemoryStream(chosen));
                    }
                }
            }
        }

        void OnLabel_Clicked(object sender, EventArgs e)
        {
            Label label = (Label)sender;
            if(label.StyleId == "4")
            {
                InviteUserPicker.SelectedItem = "Bride";
                PopUpDialog.ShowDialog();
            }
            else
            {
                InviteUserPicker.SelectedItem = "Groom";
                PopUpDialog.ShowDialog();
            }
        }

        void AddButton_Clicked(object sender, System.EventArgs e)
        {
            if (InviteUserPicker.SelectedItem.ToString() != string.Empty)
            {
                if(InviteUserPicker.SelectedItem.ToString() == "Bride")
                {
                    BrideNameLabel.Text = SearchEntry.Text;
                }
                else if(InviteUserPicker.SelectedItem.ToString() == "Groom")
                {
                    GroomNameLabel.Text = SearchEntry.Text;
                }
                int index = 0;
                if(attendees.Contains(user))
                {
                    attendees.Remove(user);
                    index = attendees.IndexOf(user);
                }
                user.role = InviteUserPicker.SelectedItem.ToString();
                attendees.Insert(index, user);
                searchUser.Remove(user);
                InviteUserPicker.SelectedItem = null;
                SearchEntry.Text = "";
            }
        }

        void SetNavigation()
        {
            this.RightIcon2 = "Close";
            this.RightButton2Command = new Command((x) => Navigation.PopModalAsync());
        }

        void SearchUser_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            //foreach (var data in searchUser)
            //{
            //    if (data.name.ToLower().Contains(((Entry)sender).Text.ToLower()))
            //    {
            //        if (!((ObservableCollection<User>)SearchList.ItemsSource).Contains(data))
            //        {
            //            ((ObservableCollection<User>)SearchList.ItemsSource).Add(data);
            //        }
            //    }
            //    else
            //    {
            //        if (((ObservableCollection<User>)SearchList.ItemsSource).Contains(data))
            //        {
            //            ((ObservableCollection<User>)SearchList.ItemsSource).Remove(data);
            //        }
            //    }
            //}

            Task.Run(async () =>
            {
                string url = Constants.ROOT_URL + Constants.USER_URL + "?token=" + Application.Current.Properties["user_token"] + "&name=" + SearchEntry.Text;
                await restService.GetData<User>(url, ct.Token);
            });
        }

        void Handle_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            SearchList.ItemsSource = new ObservableCollection<User>(searchUser);
        }

        void User_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            User selectedUser = (User)SearchList.SelectedItem;
            SearchEntry.Text = selectedUser.name;
            user = selectedUser;
            SearchList.SelectedItem = null;
        }

        public void CreateButton_Clicked(object sender, EventArgs e)
        {
            JObject newPlan = new JObject();
            JObject jObject = new JObject();
            DateTime date = StartDate.Date.Add(StartTime.Time);
            newPlan.Add("start_at", date.ToString("yyyy/MM/dd hh:mm tt"));
            newPlan.Add("reception_address", ReceptionAddressEntry.Text);
            newPlan.Add("wedding_address", WeddingAddressEntry.Text);
            newPlan.Add("description", DescriptionEntry.Text);
            newPlan.Add("officiator", OfficiatorEntry.Text);
            newPlan.Add("title", TitleEntry.Text);
            newPlan.Add("bride_name", BrideNameLabel.Text);
            newPlan.Add("groom_name", GroomNameLabel.Text);
            newPlan.Add("banner", "");
            newPlan.Add("groom_photo", "");
            newPlan.Add("bride_photo", "");

            jObject.Add("plan", newPlan);
            jObject.Add("token", Application.Current.Properties["user_token"].ToString());
            jObject.Add("role", "Wedding Planner");
            jObject.Add("attendees", JToken.FromObject(attendees));
            //plan.is_owner = 1;
            //plan.rsvp = 1;
            LoadingIndicator.IsVisible = true;
            if(isEditing == true)
            {
                //int index = Constants.planList.IndexOf(plan);
                //Constants.planList.Remove(plan);
                //Constants.planList.Insert(index, plan);
                //Navigation.PopModalAsync();
                //DependencyService.Get<iToast>().ShowMessage("Plan successfully edited!");
                List<string> xkeys = new List<string>() { "token", "role", "id" };
                List<string> keys = new List<string>() { "plan", "attendees" };
                string url = Constants.ROOT_URL + Constants.PLAN_URL + "/" + plan.id;
                jObject.Add("id", plan.id);
                Task.Run(async () =>
                {
                    await restService.MultiPartDataContentAsync(url, keys, ct.Token, true, jObject, streams, xkeys);
                });
            }
            else
            {
                List<string> xkeys = new List<string>() { "token", "role" };
                List<string> keys = new List<string>() { "plan", "attendees"};
                string url = Constants.ROOT_URL + Constants.PLAN_URL;
                Task.Run(async () =>
                {
                    //await restService.MultiPartDataContentAsync(url, keys, ct.Token, new
                    //{
                    //plan = new
                    //{
                    //    start_date = date.ToString("yyyy/MM/dd hh:mm tt"),
                    //    reception_address = ReceptionAddressEntry.Text,
                    //    wedding_address = WeddingAddressEntry.Text,
                    //    description = DescriptionEntry.Text,
                    //    officiator = OfficiatorEntry.Text,
                    //    title = TitleEntry.Text
                    //},
                    //attendees = new { attendees }, 
                    //token = Application.Current.Properties["user_token"], 
                    //role = "Wedding Planner" }, streams, xkeys);
                    await restService.MultiPartDataContentAsync(url, keys, ct.Token, false, jObject, streams, xkeys);
                });
            }
        }

        public void InviteButton_Clicked(object sender, EventArgs e)
        {
            PopUpDialog.ShowDialog();
        }

        public void DeleteClicked(object sender, EventArgs e)
        {
            var item = (Button)sender;
            User listitem = (from itm in attendees where itm.name == item.CommandParameter.ToString() select itm).FirstOrDefault<User>();
            Task.Run(async () => 
            {
                string url = Constants.ROOT_URL + Constants.PLAN_URL + "/" + plan.id + Constants.ATTENDEE_URL + "/" + listitem.id + "?token=" + Application.Current.Properties["user_token"];
                await restService.DeleteDataAsync<User>(url, ct.Token);
            });
            attendees.Remove(listitem);
        }

        public void ReceiveJSONData(JObject dataDictionary, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if ((int.Parse(dataDictionary["status"].ToString())) == 200)
                {
                    try
                    {
                        searchUser = JsonConvert.DeserializeObject<ObservableCollection<User>>(dataDictionary[isDBCalled == true ? "list" : "users"].ToString());
                        SearchList.ItemsSource = searchUser;
                    }
                    catch (Exception e) { Debug.WriteLine(e.Message); }
                    try
                    {
                        LoadingIndicator.IsVisible = false;
                        var newPlan = JsonConvert.DeserializeObject<Plan>(dataDictionary[isDBCalled == true ? "list" : "plan"].ToString());
                        Debug.WriteLine(dataDictionary);
                        Navigation.PopModalAsync();
                        if (isEditing == true)
                        {
                            DependencyService.Get<iToast>().ShowMessage("Plan succesfully edited");
                        }
                        else
                        {
                            DependencyService.Get<iToast>().ShowMessage("Plan succesfully created");
                        }

                    }
                    catch (Exception e) { Debug.WriteLine(e.Message); }
                }
            });
        }

        public void ReceiveTimeoutError(int error, string errorStr)
        {
            
        }
    }
}
