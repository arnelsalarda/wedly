﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Wedly.Models;
using Wedly.Utilities;
using Wedly.Utilities.DependecyServices;
using Wedly.Utilities.Helpers.FileReader;
using Wedly.Utilities.Helpers.RestService;
using Xamarin.Forms;

namespace Wedly.Views
{
    public partial class SigninPage : RootViewPage, iFileConnector, iRestConnector
    {
        CancellationTokenSource ct = new CancellationTokenSource();
        RestService restService = new RestService();
        FileReader fileReader = new FileReader();
        bool isDBCalled = false;

        public SigninPage()
        {
            InitializeComponent();
#if DEBUG
            fileReader.FileReaderDelegate = this;
#else
            restService.RestServiceDelegate = this;
#endif
        }

        void SigninButton_Clicked(object sender, System.EventArgs e)
        {
            Task.Run(async () =>
            {
                LoadingIndicator.IsVisible = true;
                if (EmailEntry.Text.Length != 0 && PasswordEntry.Text.Length != 0)
                {
#if DEBUG
                    await fileReader.ReadFile("Signin.json", true, ct.Token);
#else
                    await restService.CreateData<User>(Constants.ROOT_URL + Constants.SIGNIN_URL, new { user = new { email = EmailEntry.Text, password = PasswordEntry.Text }, device = new { os = 1, token = Application.Current.Properties["fcm_token"] } }, ct.Token);
#endif
                }
            });
        }

        void OnLabel_Clicked(object sender, EventArgs e)
        {
            Label label = (Label)sender;
            if (label.Text == "Sign up")
            {
                Navigation.PushAsync(new SignupPage());
            }
            else
            {
                
            }
        }

        public void ReceiveJSONData(JObject dataDictionary, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if ((int.Parse(dataDictionary["status"].ToString())) == 200)
                {
                    LoadingIndicator.IsVisible = false;
                    var registeredUser = JsonConvert.DeserializeObject<User>((dataDictionary["user"].ToString()));
                    Application.Current.Properties.Add("user_token", registeredUser.token);
                    Application.Current.Properties.Add("user_avatar", registeredUser.avatar);
                    Application.Current.Properties.Add("user_name", registeredUser.name);
                    Application.Current.SavePropertiesAsync();
                    App.Current.MainPage = new LandingPage();
                }
                else
                {
                    Debug.WriteLine((int.Parse(dataDictionary["status"].ToString())));
                }
            });
        }

        public void ReceiveTimeoutError(int error, string errorStr)
        {
            DependencyService.Get<iToast>().ShowMessage(errorStr);
        }
    }
}
