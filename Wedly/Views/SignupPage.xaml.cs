﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Wedly.Models;
using Wedly.Utilities;
using Wedly.Utilities.DependecyServices;
using Wedly.Utilities.Helpers.FileReader;
using Wedly.Utilities.Helpers.RestService;
using Xamarin.Forms;

namespace Wedly.Views
{
    public partial class SignupPage : RootViewPage, iRestConnector
    {
        CancellationTokenSource ct = new CancellationTokenSource();
        RestService restService = new RestService();
        String profilePic = "";
        Stream[] userAvatar = new MemoryStream[1];
        bool isValid;
        public SignupPage()
        {
            InitializeComponent();
            restService.RestServiceDelegate = this;
        }

        async void SignupButton_Clicked(object sender, System.EventArgs e)
        {
            isValid = true;
            if(String.IsNullOrEmpty(NameEntry.Text) || String.IsNullOrEmpty(EmailEntry.Text) || String.IsNullOrEmpty(PasswordEntry.Text) || String.IsNullOrEmpty(ConfirmPasswordEntry.Text))
            {
                isValid = false;
            }

            if(isValid)
            {
                LoadingIndicator.IsVisible = true;
                List<string> keys = new List<string>() { "user" };
                string url = Constants.ROOT_URL + Constants.REGISTER_URL;
                await restService.MultiPartDataContentAsync(url, keys, ct.Token, false, new { user = new { name = NameEntry.Text, avatar = "", email = EmailEntry.Text, password = PasswordEntry.Text, password_confirmation = ConfirmPasswordEntry.Text } }, userAvatar);
            }
        }

        void OnLabel_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        async void OnImage_Clicked(object sender, EventArgs e)
        {
            Image image = (Image)sender;
            Stream chosenImage = await DependencyService.Get<iImagePicker>().GetImageFromGalleryAsync();
            if (chosenImage != null)
            {
                using(MemoryStream ms = new MemoryStream())
                {
                    chosenImage.CopyTo(ms);
                    byte[] chosen = ms.ToArray();
                    userAvatar[0] = new MemoryStream(chosen);
                    Avatar.Source = ImageSource.FromStream(() => new MemoryStream(chosen));
                }
            }
        }

        public void ReceiveJSONData(JObject dataDictionary, CancellationToken ct)
        {
            if ((int.Parse(dataDictionary["status"].ToString())) == 200)
            {
                var registeredUser = JsonConvert.DeserializeObject<User>((dataDictionary["user"].ToString()));
                LoadingIndicator.IsVisible = false;
                Application.Current.Properties.Add("user_token", registeredUser.token);
                Application.Current.Properties.Add("user_avatar", registeredUser.avatar);
                Application.Current.Properties.Add("user_name", registeredUser.name);
                Application.Current.SavePropertiesAsync();
                App.Current.MainPage = new LandingPage();
            }
        }

        public void ReceiveTimeoutError(int error, string errorStr)
        {
            DependencyService.Get<iToast>().ShowMessage(errorStr);
        }
    }
}
