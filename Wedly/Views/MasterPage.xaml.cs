﻿using System;
using System.Collections.Generic;
using Wedly.Utilities;
using Xamarin.Forms;

namespace Wedly.Views
{
    public partial class MasterPage : RootViewPage
    {
        public MasterPage()
        {
            InitializeComponent();
            this.LeftIcon = "Close";
            this.LeftButtonCommand = new Command((x) => ((MasterDetailPage)App.Current.MainPage).IsPresented = false);
            UserAvatar.Source = Application.Current.Properties["user_avatar"].ToString();
            UserName.Text = Application.Current.Properties["user_name"].ToString();
        }

        void PlanButton_Clicked(object sender, System.EventArgs e)
        {
            ((MasterDetailPage)App.Current.MainPage).Detail = new NavigationPage(new PlansPage());
            ((MasterDetailPage)App.Current.MainPage).IsPresented = false;
        }

        void ProfileButton_Clicked(object sender, System.EventArgs e)
        {
            
        }

        void LogoutButton_Clicked(object sender, System.EventArgs e)
        {
            Application.Current.Properties.Remove("user_token");
            Application.Current.Properties.Remove("user_avatar");
            Application.Current.Properties.Remove("user_name");
            Application.Current.SavePropertiesAsync();
            App.Current.MainPage = new NavigationPage(new SigninPage());
        }
    }
}
