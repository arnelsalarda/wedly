﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using System.Threading.Tasks;
using Android.Content;
using System.IO;
using Plugin.FirebasePushNotification;
using Android.Graphics;

namespace Wedly.Droid
{
    [Activity(Label = "Wedly", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        //public static MainActivity GetInstance;
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);
            var width = Resources.DisplayMetrics.WidthPixels;
            var height = Resources.DisplayMetrics.HeightPixels;
            var density = Resources.DisplayMetrics.Density;

            App.ScreenWidth = width / density;
            App.ScreenHeight = height / density;
            App.DeviceScale = density;
            Instance = this;

            global::Xamarin.Forms.Forms.Init(this, bundle);
            initFirebase();
            LoadApplication(new App());
            FirebasePushNotificationManager.ProcessIntent(this, Intent);
        }

        //public TaskCompletionSource<string> TaskCompletionSource { set; get; }
        //public static readonly int PickImageId = 938;

        //public static MainActivity Instance
        //{
        //    get; private set;
        //}

        //protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        //{
        //    base.OnActivityResult(requestCode, resultCode, data);

        //    if (requestCode == PickImageId)
        //    {
        //        if ((resultCode == Result.Ok) && (data != null))
        //        {
        //            Android.Net.Uri uri = data.Data;
        //            Stream stream = ContentResolver.OpenInputStream(uri);
        //            System.Diagnostics.Debug.WriteLine(uri);
        //            string path = GetFilePath(uri);
        //            TaskCompletionSource.SetResult(path);
        //        }
        //        else
        //        {
        //            TaskCompletionSource.SetResult(null);
        //        }
        //    }
        //}

        //private string GetFilePath(Android.Net.Uri uri)
        //{
        //    string doc_id = "";
        //    using (var c1 = ContentResolver.Query(uri, null, null, null, null))
        //    {
        //        c1.MoveToFirst();
        //        string document_id = c1.GetString(0);
        //        doc_id = document_id.Substring(document_id.LastIndexOf(":") + 1);
        //    }

        //    string path = null;

        //    if (doc_id.EndsWith(".jpeg") || doc_id.EndsWith(".png") || doc_id.EndsWith(".jpg"))
        //    {
        //        return doc_id;
        //    }

        //    // The projection contains the columns we want to return in our query.
        //    string selection = Android.Provider.MediaStore.Images.Media.InterfaceConsts.Id + " =? ";
        //    using (var cursor = ContentResolver.Query(Android.Provider.MediaStore.Images.Media.ExternalContentUri, null, selection, new string[] { doc_id }, null))
        //    {
        //        if (cursor == null) return path;
        //        var columnIndex = cursor.GetColumnIndexOrThrow(Android.Provider.MediaStore.Images.Media.InterfaceConsts.Data);
        //        cursor.MoveToFirst();
        //        path = cursor.GetString(columnIndex);
        //    }
        //    return path;
        //}

        public TaskCompletionSource<Stream> TaskCompletionSource { set; get; }
        public static readonly int PickImageId = 938;

        public static MainActivity Instance
        {
            get; private set;
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == PickImageId)
            {
                if (resultCode == Result.Ok && data != null)
                {
                    Android.Net.Uri imgUri = data.Data;
                    Stream imgStream = ContentResolver.OpenInputStream(imgUri);

                    //var dataStream = ResizeImage(StreamToByteArray(imgStream), 1000, 1000);

                    //TaskCompletionSource.SetResult(new MemoryStream(dataStream));
                    TaskCompletionSource.SetResult(imgStream);
                }
                else
                {
                    TaskCompletionSource.SetResult(null);
                }
            }
        }

        static byte[] StreamToByteArray(Stream stream)
        {
            using (stream)
            {
                using (MemoryStream memStream = new MemoryStream())
                {
                    stream.CopyTo(memStream);
                    return memStream.ToArray();
                }
            }
        }

        public byte[] ResizeImage(byte[] imageData, float width, float height)
        {
            // Load the bitmap 
            BitmapFactory.Options options = new BitmapFactory.Options();// Create object of bitmapfactory's option method for further option use
            options.InPurgeable = true; // inPurgeable is used to free up memory while required
            Bitmap originalImage = BitmapFactory.DecodeByteArray(imageData, 0, imageData.Length, options);

            Bitmap resizedImage = Bitmap.CreateScaledBitmap(originalImage, (int)width, (int)height, true);

            originalImage.Recycle();

            using (MemoryStream ms = new MemoryStream())
            {
                resizedImage.Compress(Bitmap.CompressFormat.Png, 100, ms);

                resizedImage.Recycle();

                return ms.ToArray();
            }
        }

        void initFirebase()
        {
            //Set the default notification channel for your app when running Android Oreo
            if (Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.O)
            {
                //Change for your default notification channel id here
                FirebasePushNotificationManager.DefaultNotificationChannelId = "FirebasePushNotificationChannel";

                //Change for your default notification channel name here
                FirebasePushNotificationManager.DefaultNotificationChannelName = "General";
            }

            //If debug you should reset the token each time.
#if DEBUG
            FirebasePushNotificationManager.Initialize(this, false, true);
#else
                  FirebasePushNotificationManager.Initialize(this,false);
#endif

            CrossFirebasePushNotification.Current.RegisterForPushNotifications();

            CrossFirebasePushNotification.Current.OnTokenRefresh += (source, e) => {
                System.Diagnostics.Debug.WriteLine("NEW TOKEN: " + e.Token);
                Xamarin.Forms.Application.Current.Properties.Add("fcm_token", e.Token);
                Xamarin.Forms.Application.Current.SavePropertiesAsync();
            };


            //Handle notification when app is closed here
            CrossFirebasePushNotification.Current.OnNotificationReceived += (s, p) =>
            {

                System.Diagnostics.Debug.WriteLine("Gwapo message: " + p.Data);
            };

        }

        protected override void OnNewIntent(Intent intent)
        {
            base.OnNewIntent(intent);
            FirebasePushNotificationManager.ProcessIntent(this, intent);
        }
    }
}

