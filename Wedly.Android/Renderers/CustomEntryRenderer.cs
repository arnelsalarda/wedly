﻿using System;
using Android.Content;
using Android.Views;
using Wedly.Droid.Renderers;
using Wedly.Utilities.CustomRenderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace Wedly.Droid.Renderers
{
    public class CustomEntryRenderer : EntryRenderer
    {
         
        public CustomEntryRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                var customEntry = e.NewElement as CustomEntry;
                if (customEntry.TextAlignment == "Center")
                {
                    Control.Gravity = GravityFlags.CenterHorizontal;
                }
                Control.Background = null;
                Control.SetPadding(0, 0, 0, 0);
            }
        }
    }
}
