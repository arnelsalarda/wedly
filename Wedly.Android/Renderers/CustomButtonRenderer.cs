﻿using System;
using Android.Content;
using Wedly.Droid.Renderers;
using Wedly.Utilities.CustomRenderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomButton), typeof(CustomButtonRenderer))]
namespace Wedly.Droid.Renderers
{
    public class CustomButtonRenderer : ButtonRenderer
    {
        //CustomButton customButton;
        public CustomButtonRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                var customButton = e.NewElement as CustomButton;
                if(customButton.TextCasing == "Pascal")
                {
                    Control.SetAllCaps(false);
                }

                if(customButton.TextPadding == "Zero")
                {
                    //Control.Gravity = Android.Views.GravityFlags.Left | Android.Views.GravityFlags.Top;
                    Control.SetPadding(0, 0, 0, 0);
                }
            }
        }
    }
}
