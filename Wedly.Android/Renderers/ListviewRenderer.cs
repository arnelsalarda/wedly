﻿using System;
using Android.Content;
using Wedly.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ListView), typeof(ListviewRenderer))]
namespace Wedly.Droid.Renderers
{
    public class ListviewRenderer : ListViewRenderer
    {
        public ListviewRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
        {
            base.OnElementChanged(e);
            if (e.NewElement != null)
            {
                var listView = this.Control as Android.Widget.ListView;
                listView.NestedScrollingEnabled = true;               
            }
        }
    }
}
