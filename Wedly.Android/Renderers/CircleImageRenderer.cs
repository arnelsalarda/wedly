﻿using System;
using Android.Content;
using Android.Graphics;
using Wedly.Droid.Renderers;
using Wedly.Utilities.CustomRenderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: Xamarin.Forms.ExportRenderer(typeof(CircleImage), typeof(CircleImageRenderer))]
namespace Wedly.Droid.Renderers
{
    public class CircleImageRenderer : ImageRenderer
    {
        public CircleImageRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Image> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement == null)
            {
                if ((int)Android.OS.Build.VERSION.SdkInt < 18)
                {
                    SetLayerType(LayerType, null);
                }
            }
        }

        protected override bool DrawChild(Canvas canvas, global::Android.Views.View child, long drawingTime)
        {
            //try
            //{
            //    var radius = Math.Min(Width, Height) / 2;
            //    var strokeWidth = 10;
            //    radius -= strokeWidth / 2;

            //    //Create path to clip
            //    var path = new Path();
            //    path.AddCircle(Width / 2, Height / 2, radius, Path.Direction.Ccw);
            //    canvas.Save();
            //    canvas.ClipPath(path);

            //    var result = base.DrawChild(canvas, child, drawingTime);

            //    canvas.Restore();

            //    // Create path for circle border
            //    path = new Path();
            //    path.AddCircle(Width / 2, Height / 2, radius, Path.Direction.Ccw);

            //    var paint = new Paint();
            //    paint.AntiAlias = true;
            //    paint.StrokeWidth = 2;
            //    paint.SetStyle(Paint.Style.Stroke);
            //    paint.Color = global::Android.Graphics.Color.White;

            //    canvas.DrawPath(path, paint);

            //    //Properly dispose
            //    paint.Dispose();
            //    path.Dispose();
            //    return result;
            //}
            //catch (Exception ex)
            //{
            //    System.Diagnostics.Debug.WriteLine("Unable to create circle image: " + ex);
            //}

            //return base.DrawChild(canvas, child, drawingTime);

            try
            {

                var radius = (float)Math.Min(Width, Height) / 2f;

                var borderThickness = ((CircleImage)Element).BorderThickness;

                float strokeWidth = 0f;

                if (borderThickness > 0)
                {
                    var logicalDensity = Xamarin.Forms.Forms.Context.Resources.DisplayMetrics.Density;
                    strokeWidth = (float)Math.Ceiling(borderThickness * logicalDensity + .5f);
                }

                radius -= strokeWidth / 2f;




                var path = new Path();
                path.AddCircle(Width / 2.0f, Height / 2.0f, radius, Path.Direction.Ccw);


                canvas.Save();
                canvas.ClipPath(path);



                var paint = new Paint
                {
                    AntiAlias = true
                };
                paint.SetStyle(Paint.Style.Fill);
                paint.Color = ((CircleImage)Element).FillColor.ToAndroid();
                canvas.DrawPath(path, paint);
                paint.Dispose();


                var result = base.DrawChild(canvas, child, drawingTime);

                path.Dispose();
                canvas.Restore();

                path = new Path();
                path.AddCircle(Width / 2f, Height / 2f, radius, Path.Direction.Ccw);


                if (strokeWidth > 0.0f)
                {
                    paint = new Paint
                    {
                        AntiAlias = true,
                        StrokeWidth = strokeWidth
                    };
                    paint.SetStyle(Paint.Style.Stroke);
                    paint.Color = ((CircleImage)Element).BorderTint.ToAndroid();
                    canvas.DrawPath(path, paint);
                    paint.Dispose();
                }

                path.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Unable to create circle image: " + ex);
            }

            return base.DrawChild(canvas, child, drawingTime);
        }
    }
}
