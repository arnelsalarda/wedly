﻿using System;
using Android.Content;
using Wedly.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android.AppCompat;

[assembly: ExportRenderer(typeof(Xamarin.Forms.MasterDetailPage), typeof(MasterDetailRenderer))]
namespace Wedly.Droid.Renderers
{
    public class MasterDetailRenderer : MasterDetailPageRenderer
    {
        bool firstDone;

        public MasterDetailRenderer(Context context) : base(context)
        {
        }

        public override void AddView(Android.Views.View child)
        {
            if (firstDone)
            {
                var metrics = Resources.DisplayMetrics;
                LayoutParams p = (LayoutParams)child.LayoutParameters;
                p.Width = metrics.WidthPixels;
                base.AddView(child, p);
            }
            else
            {
                firstDone = true;
                base.AddView(child);
            }

        }
    }
}
