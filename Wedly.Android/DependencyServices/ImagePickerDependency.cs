﻿using System;
using System.IO;
using System.Threading.Tasks;
using Android;
using Android.Content;
using Android.Content.PM;
using Android.Database;
using Android.Provider;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Wedly.Droid.DependencyServices;
using Wedly.Utilities.DependecyServices;
using Xamarin.Forms;

[assembly: Dependency(typeof(ImagePickerDependency))]
namespace Wedly.Droid.DependencyServices
{
    public class ImagePickerDependency : iImagePicker
    {
        
        //public Task<string> GetImageFromGalleryAsync()
        //{

        //    int MY_PERMISSIONS_REQUEST_Location = 103;
        //    if (ContextCompat.CheckSelfPermission(MainActivity.GetInstance, Manifest.Permission.ReadExternalStorage) != Permission.Granted)
        //    {
        //        if (ActivityCompat.ShouldShowRequestPermissionRationale(MainActivity.GetInstance, Manifest.Permission.ReadExternalStorage))
        //        {
        //        }
        //        else
        //        {
        //            ActivityCompat.RequestPermissions(MainActivity.GetInstance, new String[] { Manifest.Permission.ReadExternalStorage }, MY_PERMISSIONS_REQUEST_Location);
        //        }
        //    }

        //    Intent intent = new Intent();
        //    intent.SetType("image/*");
        //    intent.SetAction(Intent.ActionGetContent);

        //    MainActivity activity = Forms.Context as MainActivity;

        //    activity.StartActivityForResult(Intent.CreateChooser(intent, "Select Picture"), MainActivity.PickImageId);

        //    activity.TaskCompletionSource = new TaskCompletionSource<string>();

        //    return activity.TaskCompletionSource.Task;
        //}

        public Task<Stream> GetImageFromGalleryAsync()
        {
            int MY_PERMISSIONS_REQUEST_Location = 103;
            if (ContextCompat.CheckSelfPermission(MainActivity.Instance, Manifest.Permission.ReadExternalStorage) != Permission.Granted)
            {
                if (ActivityCompat.ShouldShowRequestPermissionRationale(MainActivity.Instance, Manifest.Permission.ReadExternalStorage))
                {
                }
                else
                {
                    ActivityCompat.RequestPermissions(MainActivity.Instance, new String[] { Manifest.Permission.ReadExternalStorage }, MY_PERMISSIONS_REQUEST_Location);
                }
            }

            Intent intent = new Intent();
            intent.SetType("image/*");
            intent.SetAction(Intent.ActionGetContent);

            MainActivity.Instance.StartActivityForResult(Intent.CreateChooser(intent, "Get Image from:"), MainActivity.PickImageId);

            MainActivity.Instance.TaskCompletionSource = new TaskCompletionSource<Stream>();

            return MainActivity.Instance.TaskCompletionSource.Task;
        }
    }
}
