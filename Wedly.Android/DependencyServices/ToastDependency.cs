﻿using System;
using Wedly.Droid.DependencyServices;
using Wedly.Utilities.DependecyServices;
using Xamarin.Forms;

[assembly: Dependency(typeof(ToastDependency))]
namespace Wedly.Droid.DependencyServices
{
	public class ToastDependency : iToast
    {
        public void ShowMessage(string message)
        {
            Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Long).Show();
        }
    }
}
